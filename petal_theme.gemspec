$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "petal_theme/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "petal_theme"
  spec.version     = PetalTheme::VERSION
  spec.authors     = ["elijah"]
  spec.email       = ["elijah@calyxinstitute.org"]
  spec.homepage    = "https://calyxinstitute.org"
  spec.summary     = "Theme for Calyx Institute"
  spec.description = "Theme for Calyx Institute"
  spec.license     = "MIT"
  spec.metadata["allowed_push_host"] = "https://0xacab.org"

  spec.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.md"]
end
