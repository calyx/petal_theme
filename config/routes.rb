PetalTheme::Engine.routes.draw do
  root "test#show"
  get ':page', to: "test#show", format: "html"
end
