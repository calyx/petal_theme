# PetalTheme

This gem provides the theme for Calyx Institute websites.

## Links

Open source icon collections:

* https://fonts.google.com/icons
* https://useiconic.com/open
* https://feathericons.com/

Free icons:

* https://lineicons.com/icons/
* https://icons8.com/line-awesome
* https://fontawesome.com/
* https://www.glyphicons.com/tools/social/
