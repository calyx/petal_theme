module PetalTheme
  class Engine < ::Rails::Engine
    isolate_namespace PetalTheme

    initializer "petal_theme.assets.precompile" do |app|
      asset_base = File.expand_path('../../../app/assets/', __FILE__)
      asset_subdirs = ['images', 'javascripts', 'stylesheets', 'fonts']
      asset_subdirs.each do |dir|
        asset_dir   = asset_base + '/' + dir + '/'
        find_assets = asset_dir + '**/*.{svg,png,ico,jpg,wroff2,js}'
        find_css    = asset_dir + '**/[^_]*.{css,scss}'
        app.config.assets.precompile += Dir[find_assets, find_css].map {|path|
          path.sub(asset_dir, '').sub(/\.scss$/, '.css')
        }
      end
    end
  end
end
