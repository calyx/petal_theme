module PetalTheme
  class TestController < ApplicationController
    layout 'application'

    def show
      @theme_title = "Test Petal Theme"
      @theme_icons = true
      @theme_top_tabs = [
        ["/", "Home"],
        ["/about", "About Us"],
        ["/docs", "Documentation"],
        ["/research", "Research"],
        ["/account", "Account"]
      ]
      @theme_active_tab = "/" + params[:page].to_s
      if params[:page] == "account"
        @theme_top_tabs = nil
        @theme_session_content = render_to_string partial: "layouts/petal_theme/example_session_content"
      end
    end
  end
end
