module PetalTheme
  module ApplicationHelper
    def theme_title
      if @theme_title
        @theme_title
      else
        site_name = @theme_site_name || request.domain
        if controller_name != "root"
          "%s - %s" % [controller_name.titleize, site_name]
        else
          site_name
        end
      end
    end
  end
end
